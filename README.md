Life Cycle HPC
==============

This Project aims at developing a model of complex and sensible ecosytems to help parc management.

In its current version it's a simple attempt to reproduce the classical Wolf/Sheep predation model as it has been already implemented [in netlogo](http://ccl.northwestern.edu/netlogo/models/WolfSheepPredation).
Our version of the Wolf Sheep predation is based on the [Wilensky's implementation](http://ccl.northwestern.edu/netlogo/models/WolfSheepPredation) and [pandora](https://github.com/xrubio/pandora/), an ABM framework allowing to take advantage of HPC facilities.

Structure of the project :

- `src/`:  Source code of the model
- `doc/`:  Documentation and bibliography
- `utils/`:  Related content

1. [Install](doc/INSTALL.md)
