
#ifndef __Lion_hxx__
#define __Lion_hxx__

#include <Animal.hxx>
#include <Action.hxx>

#include <string>

namespace Examples
{

class Lion : public Animal
{
	int _resources; // MpiBasicAttribute

public:
	// todo remove environment from here
	Lion( const std::string & id,int energy );
	Lion( const std::string & id,Lion * parent );
	virtual ~Lion();
	
	void selectActions();
	void reproduce();


};

} // namespace Examples

#endif // __Lion_hxx__

