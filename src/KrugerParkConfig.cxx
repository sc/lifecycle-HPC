
#include <KrugerParkConfig.hxx>

namespace Examples
{

KrugerParkConfig::KrugerParkConfig( const std::string & xmlFile ) : Config(xmlFile), _lionNumber(0),_impalaNumber(0)
{
}

KrugerParkConfig::~KrugerParkConfig()
{
}

void KrugerParkConfig::loadParams()
{
	_lionNumber = getParamInt( "lion", "number");
	_impalaNumber = getParamInt( "impala", "number");

	_lionReproduce = getParamInt( "lion", "reproduce");
	_impalaReproduce = getParamInt( "impala", "reproduce");

	_lionGainFromFood = getParamInt( "lion", "gainFromFood");
	_impalaGainFromFood = getParamInt( "impala", "gainFromFood");

	_impalaSdSpeed = getParamInt( "impala", "sdSpeed");
	_impalaMeanSpeed = getParamInt( "impala", "meanSpeed");

	_lionSdSpeed = getParamInt( "lion", "sdSpeed");
	_lionMeanSpeed = getParamInt( "lion", "meanSpeed");

        _impalaSdLifeExpectancy = getParamInt( "impala", "sdLifeExpectancy");
        _impalaMeanLifeExpectancy = getParamInt( "impala", "meanLifeExpectancy");

        _lionSdLifeExpectancy = getParamInt( "lion", "sdLifeExpectancy");
        _lionMeanLifeExpectancy = getParamInt( "lion", "meanLifeExpectancy");

        _grassGrowthRate = getParamInt( "rasters/resources", "growthRate");
}
	
} // namespace Examples

