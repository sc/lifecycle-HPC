
#include <MoveAction.hxx>

#include <World.hxx>
#include <Animal.hxx>
#include <GeneralState.hxx>

namespace Examples
{

MoveAction::MoveAction()
{
}

MoveAction::~MoveAction()
{
}

void MoveAction::execute( Engine::Agent & agent )
{	
	Engine::World * world = agent.getWorld();
	Animal & animalAgent = (Animal&)agent;

	
	Engine::Point2D<int> newPosition = animalAgent.getPosition();
	int speed=animalAgent.getSpeed();

	int modX = Engine::GeneralState::statistics().getUniformDistValue(-speed,speed);
	newPosition._x += modX;
	int modY = Engine::GeneralState::statistics().getUniformDistValue(-speed,speed);
	newPosition._y += modY;

	if(world->checkPosition(newPosition))
	{
		animalAgent.setPosition(newPosition);
	}
}

std::string MoveAction::describe() const
{
	return "MoveAction";
}

} // namespace Examples

