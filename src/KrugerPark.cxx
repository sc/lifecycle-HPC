
#include <KrugerPark.hxx>

#include <KrugerParkConfig.hxx>
#include <Lion.hxx>
#include <Impala.hxx>
#include <DynamicRaster.hxx>
#include <Point2D.hxx>
#include <GeneralState.hxx>
#include <Scheduler.hxx>
#include <Logger.hxx>

namespace Examples 
{

KrugerPark::KrugerPark(Engine::Config * config, Engine::Scheduler * scheduler ) : World(config, scheduler, true)
{
}

KrugerPark::~KrugerPark()
{
}

void KrugerPark::createRasters()
{
    const KrugerParkConfig & kpConfig = (const KrugerParkConfig&)getConfig(); //read the config file
	registerDynamicRaster("grass", true);
	getDynamicRaster("grass").setInitValues(-1*kpConfig._grassGrowthRate-1, 1, 1);

	registerDynamicRaster("meat", true);
	getDynamicRaster("meat").setInitValues(0, 1, 0);

	//for(auto index:getBoundaries())
	//{
	//	int value = Engine::GeneralState::statistics().getUniformDistValue(0,1);
	//	setMaxValue("grass", index, value);
	//}
	//updateRasterToMaxValues("grass");

}

void KrugerPark::createAgents()
{
    std::stringstream logName;
	logName << "agents_" << getId();

	//Loops to generate  and add the new agents
    const KrugerParkConfig & kpConfig = (const KrugerParkConfig&)getConfig(); //read the config file
	for(int i=0; i<kpConfig._lionNumber; i++)
	{
		if((i%getNumTasks())==getId())
		{
			std::ostringstream oss;

			int initialEnergy = Engine::GeneralState::statistics().getUniformDistValue(0,2*kpConfig._lionGainFromFood); // this is what they use to initialise there agent in wilensky et al
			oss << "Lion_" << i;
			Lion * lion = new Lion(oss.str(),initialEnergy);
			addAgent(lion);
			int speed=Engine::GeneralState::statistics().getNormalDistValue(kpConfig._lionMeanSpeed,kpConfig._lionSdSpeed);
                        int lifeExpectancy=Engine::GeneralState::statistics().getNormalDistValue(kpConfig._lionMeanLifeExpectancy,kpConfig._lionSdLifeExpectancy);

			lion->setSpeed(1);
                        lion->setLifeExpectancy(lifeExpectancy);
                        lion->setGainFromFood(kpConfig._lionGainFromFood);
			lion->setRandomPosition();
			lion->setReproduce(kpConfig._lionReproduce);
	        log_INFO(logName.str(), getWallTime() << " new lion rooooaah: " << lion);
		}
	}
	for(int i=0; i<kpConfig._impalaNumber; i++)
	{
		if((i%getNumTasks())==getId())
		{
			std::ostringstream oss;
			int initialEnergy = Engine::GeneralState::statistics().getUniformDistValue(0,2*kpConfig._impalaGainFromFood); // this is what they use to initialise there agent in wilensky et al
			oss << "Impala_" << i;
			Impala * impala = new Impala(oss.str(),initialEnergy);
			addAgent(impala);

			int speed=Engine::GeneralState::statistics().getNormalDistValue(kpConfig._impalaMeanSpeed,kpConfig._impalaSdSpeed);
                        int lifeExpectancy=Engine::GeneralState::statistics().getNormalDistValue(kpConfig._impalaMeanLifeExpectancy,kpConfig._impalaSdLifeExpectancy);


			impala->setSpeed(1);
                        impala->setLifeExpectancy(lifeExpectancy);
                        impala->setGainFromFood(kpConfig._impalaGainFromFood);
			impala->setRandomPosition();
			impala->setReproduce(kpConfig._impalaReproduce);
	        log_INFO(logName.str(), getWallTime() << " new impala uuuuhuhiu: " << impala);
		}
	}
}


void KrugerPark::step()
{
	std::stringstream logName;
	logName << "simulation_" << getId();
	log_INFO(logName.str(), getWallTime() << " executing step: " << _step );

	if(_step%_config->getSerializeResolution()==0)
	{
		_scheduler->serializeRasters(_step);
		_scheduler->serializeAgents(_step);
		log_DEBUG(logName.str(), getWallTime() << " step: " << _step << " serialization done");
	}
	stepEnvironment();

	log_DEBUG(logName.str(), getWallTime() << " step: " << _step << " has executed step environment");
	_scheduler->executeAgents();
	_scheduler->removeAgents();
	log_INFO(logName.str(), getWallTime() << " finished step: " << _step);
}

void KrugerPark::stepEnvironment()
{
	for(size_t d=0; d<_rasters.size(); d++)
	{
		//if(!_rasters.at(d) || !_dynamicRasters.at(d))
		//{
		//	continue;
		//}
		// TODO initial pos and matrix size are needed?
		//stepRaster(d);
	}

	for(auto index : getBoundaries()){
	    int grassState=getValue("grass",index);
	    if(grassState == 0)setValue("grass",index,getDynamicRaster("grass").getMinValue()) ; //whe the grass is at th
	    if(grassState== -1)setValue("grass",index,1) ;
	    if(grassState < -1 )setValue("grass",index,grassState+1) ; //whe the grass is at th
	}
}
} // namespace Examples

