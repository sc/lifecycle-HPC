
#ifndef __KrugerPark_hxx
#define __KrugerPark_hxx

#include <World.hxx>

namespace Examples 
{

class KrugerParkConfig;

class KrugerPark : public Engine::World
{
	void createRasters();
	void createAgents();
public:
	KrugerPark(Engine::Config * config, Engine::Scheduler * scheduler = 0);
	void step();
	void stepEnvironment();
	virtual ~KrugerPark();
};

} // namespace Examples 

#endif // __KrugerPark_hxx

