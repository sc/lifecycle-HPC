
#ifndef __Impala_hxx__
#define __Impala_hxx__

#include <Animal.hxx>
#include <Action.hxx>

#include <string>

namespace Examples
{

class Impala : public Animal
{
	int _resources; // MpiBasicAttribute

public:
	// todo remove environment from here
	Impala( const std::string & id,int energy );
	Impala( const std::string & id,Impala * parent );
	virtual ~Impala();
	
	void selectActions();

	void reproduce();

};

} // namespace Examples

#endif // __Impala_hxx__

