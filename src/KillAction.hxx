
#ifndef __KillHomeAction_hxx__
#define __KillHomeAction_hxx__

#include <Action.hxx>
#include <string>

namespace Engine
{
	class Agent;
}

namespace Examples
{

class KillAction : public Engine::Action
{
public:
	KillAction();
	virtual ~KillAction();
	void execute( Engine::Agent & agent );
	std::string describe() const;
};

} // namespace Examples

#endif // __KillHomeAction_hxx__

