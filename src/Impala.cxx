
#include <Impala.hxx>
#include <EatAction.hxx>
#include <MoveAction.hxx>
#include <Statistics.hxx>
#include <World.hxx>

namespace Examples
{

Impala::Impala( const std::string & id,int energy ) : Animal(id,energy)
{
}

Impala::Impala( const std::string & id,Impala * parent) : Animal(id)
{

    _numOffspring=0;
    _speed=parent->getSpeed();    //this is the factor by which an animal can move at each timestep
    _lifeExpectancy=parent->getLifeExpectancy();    //this is the factor by which an animal can die after x timesteps
    _energyGain=parent->getGainFromFood(); //this is how much energie an animal will gain when he will eat 
    _visionLength=parent->getVisionLength(); //distance at wich the animal can see
    _gainFromFood=parent->getGainFromFood(); //distance at wich the animal can see
    _foodRaster=parent->getFoodRaster(); //determine in which raster this animal can eat (ie: grass for impalas, meat for lion)
    _reproduce=parent->getReproduce();//as in the wilensky model, this is the probability of reproduce at each time step

}

Impala::~Impala()
{
}

void Impala::selectActions()
{
	_actions.push_back(new MoveAction());
	_actions.push_back(new EatAction("grass"));
}

void Impala::reproduce()
{
	std::ostringstream oss;
	oss << getId() << "-"<<_numOffspring;
	Impala * child = new Impala(oss.str(),this);
	child->setEnergy(_energy/2);
	_energy=_energy/2; //the energy is splitted between parent/offspring as in wilensky et al

	_world->addAgent(child); //ad the new agent to the world
//	std::cout<<"felicitation vous avez un petit bebe Impala "<<child->getId()<<std::endl;
	child->setRandomPosition();
	_numOffspring++; //on child more
}

} // namespace Examples

