
#ifndef __Animal_hxx__
#define __Animal_hxx__

#include <Agent.hxx>
#include <Action.hxx>

#include <string>

namespace Examples
{

class Animal : public Engine::Agent
{
    protected:
	int _energy; // MpiBasicAttribute
	int _speed;    //this is the factor by which an animal can move at each timestep
        int _lifeExpectancy;    //this is the factor by which an animal can die after x timesteps
	int _energyGain;  //this is how much energie an animal will gain when he will eat 
	int _visionLength; //distance at wich the animal can see
	int _gainFromFood; //distance at wich the animal can see
	std::string _foodRaster; //determine in which raster this animal can eat (ie: grass for impalas, meat for lion)
	int _reproduce;//as in the wilensky model, this is the probability of reproduce at each time step
	int _numOffspring;//now I just use it to create unique id for children, could be more usefull in the future (to control fertility etc..) 

public:
	// todo remove environment from here
	Animal( const std::string & id );
	Animal( const std::string & id , int energy);
	Animal( const std::string & id , Animal * parent);

	virtual ~Animal();
	
	void selectActions();
	void updateState();
	void registerAttributes();
	void serialize();

	virtual void reproduce();//create a new agent from the original one and add it to the world

	void setSpeed(int);
	int getSpeed() const;

        void setLifeExpectancy(int);
        int getLifeExpectancy() const;

	void setEnergy( int energy );
	int getEnergy() const;

	void setGainFromFood( int gainFromFood ); //this told the level of energy the animal gain when he eats
	int getGainFromFood() const;

	int getLifeExpectancy(){ return _lifeExpectancy;}
	int getVisionLength(){ return _visionLength;}
	std::string getFoodRaster(){ return _foodRaster;}
	int getReproduce(){ return _reproduce;}
	int setReproduce(int reproduce){ _reproduce = reproduce;}

	void killed();


	////////////////////////////////////////////////
	// This code has been automatically generated //
	/////// Please do not modify it ////////////////
	////////////////////////////////////////////////
	Animal( void * );
	void * fillPackage();
	void sendVectorAttributes(int);
	void receiveVectorAttributes(int);
	////////////////////////////////////////////////
	//////// End of generated code /////////////////
	////////////////////////////////////////////////

};

} // namespace Examples

#endif // __Animal_hxx__

