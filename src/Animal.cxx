
#include <Animal.hxx>
#include <EatAction.hxx>
#include <MoveAction.hxx>
#include <Statistics.hxx>
#include <World.hxx>
#include <GeneralState.hxx>

namespace Examples
{

Animal::Animal( const std::string & id ) : Agent(id), _energy(200)
{
}
Animal::Animal( const std::string & id, int energy ) : Agent(id)
{
    _energy=energy;
    _numOffspring=0;
}

Animal::Animal( const std::string & id, Animal *parent) : Agent(id)
{
    _numOffspring=0;
    _speed=parent->getSpeed();    //this is the factor by which an animal can move at each timestep
    _lifeExpectancy=parent->getLifeExpectancy();    //this is the factor by which an animal can die after x timesteps
    _energyGain=parent->getGainFromFood(); //this is how much energie an animal will gain when he will eat 
    _visionLength=parent->getVisionLength(); //distance at wich the animal can see
    _gainFromFood=parent->getGainFromFood(); //distance at wich the animal can see
    _foodRaster=parent->getFoodRaster(); //determine in which raster this animal can eat (ie: grass for impalas, meat for lion)
    _reproduce=parent->getReproduce();//as in the wilensky model, this is the probability of reproduce at each time step
}

Animal::~Animal()
{
}

void Animal::selectActions()
{
	_actions.push_back(new MoveAction());
	_actions.push_back(new EatAction(_foodRaster));
}

void Animal::updateState()
{
	if(_lifeExpectancy-1 < 0){ //if you go over your life expectancy, sorry you dead
	    std::cout<<"ur too old  mthfckz "<<getId()<<std::endl;
	    _world->removeAgent(this);
	}
	else if(_energy<0) //if you don't have more energy, sorry you dead
	{
	    std::cout<<"u dead by starvation mthfckz "<<getId()<<std::endl;
	    _world->removeAgent(this);
	}
	else if(!_exists){//if you are already dead, pleas, die for real! (this happen if someone killed you, but yet you didn't realized)
	    std::cout<<" strange circunstance  but dead anyway dude "<<getId()<<std::endl;
	    _world->removeAgent(this);
	}
	else{ //lucky you, you are still alive: you have the right to get older and hungrier. And maybe to have child that  in turn will get older and die
	    _lifeExpectancy--;
	    _energy--;
	    double rand_num=(double)(Engine::GeneralState::statistics().getUniformDistValue(0,RAND_MAX))/(double)(RAND_MAX); //we use the GeneralState classe to generate a random nubmer as with the paraliisation using a simple seed is not enough.

		if(rand_num<double(_reproduce)/100.0){ //if the random number is less than the reproduction proba then
		reproduce(); //let's do this f*** thing
		
	    }
	}
}

void Animal::reproduce()
{
	_numOffspring++; //on child more
	std::ostringstream oss;
	oss << getId() << "-"<<_numOffspring;
	Animal * child = new Animal(oss.str(),this);
	child->setEnergy(_energy/2);
	_energy=_energy/2; //the energy is splitted between parent/offspring as in wilensky et al

	_world->addAgent(child); //ad the new agent to the world
	std::cout<<"felicitation vous avez un petit bebe Animal "<<child->getId()<<std::endl;
	child->setPosition(getPosition());
}


void Animal::registerAttributes()
{
	registerIntAttribute("energy");
}

void Animal::serialize()
{
	serializeAttribute("energy", _energy);
}

void Animal::setEnergy( int energy )
{
	_energy = energy;
}

int Animal::getEnergy() const
{
	return _energy;
}
//    speed code---------------
int Animal::getSpeed() const
{
	return _speed;
}

void Animal::setSpeed( int speed )
{
	_speed = speed;
}
//    lifeExpectancy code----------------
int Animal::getLifeExpectancy() const
{
        return _lifeExpectancy;
}

void Animal::setLifeExpectancy( int lifeExpectancy )
{
        _lifeExpectancy = lifeExpectancy;
}


void Animal::setGainFromFood( int gainFromFood )
{
	_gainFromFood = gainFromFood;
}

int Animal::getGainFromFood() const
{
	return _gainFromFood;
}

void Animal::killed(){
	    _exists=0;
}
} // namespace Examples

