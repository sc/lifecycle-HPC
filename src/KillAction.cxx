
#include <KillAction.hxx>

#include <World.hxx>
#include <Animal.hxx>
#include <Impala.hxx>
#include <GeneralState.hxx>

namespace Examples
{

KillAction::KillAction()
{
}

KillAction::~KillAction()
{
}

void KillAction::execute( Engine::Agent & agent )
{	
	Engine::World * world = agent.getWorld();
	Animal & animalAgent = (Animal&)agent;


	//if the lion have some neighbours we check if they are impalas, if yes they eat one 
	//std::cout<<" try to eat "<<animalAgent.getId()<<std::endl;
	if(world->countNeighbours(&agent,2) >= 1){
		Engine::AgentsVector preys = world->getNeighbours(& agent,2);
		Engine::AgentsVector::iterator prey = preys.begin();
		bool done=false;
		//check if among the neighbours there are some impalas
		while(prey!=preys.end()  && !done  ){
			Engine::Agent* p=(prey->get());
			//std::cout<<" king "<<animalAgent.getId()<<" je vais peut etre me bouffer: " <<p->getId()<<std::endl;
			if (Impala * i = dynamic_cast<Impala*>(p)) { //dynamic cas from Agent to => impala to see if it's an impala
			    	i->killed();
				world->setValue("meat", animalAgent.getPosition(),1); //the meat of the impala is stored in the meat raster and available to be eaten
				done=true;
			} 
			prey ++ ;
		}
	}

}

std::string KillAction::describe() const
{
	return "KillAction";
}

} // namespace Examples

