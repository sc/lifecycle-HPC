
#include <EatAction.hxx>

#include <World.hxx>
#include <Animal.hxx>

namespace Examples
{

EatAction::EatAction(std::string rasterId)
{
    _rasterId=rasterId;
}

EatAction::~EatAction()
{
}

EatAction::EatAction():EatAction("grass")
{
}

void EatAction::execute( Engine::Agent & agent)
{
	Animal & simpleAgent = (Animal&)agent; 
	Engine::World * world = agent.getWorld(); 

	int myEnergy=simpleAgent.getEnergy(); //my energy before eating
	int energyAvailable = world->getValue(_rasterId, agent.getPosition()); //the energy available on the raster

	//So far the information stored in the resources raster are binary: there is or not food. If ther is then you can eat and you gain getGainFromFood and the raster value is 0.
	if(energyAvailable >0){

	    int energyGathered = 0; // the energy that we can gather
	     energyGathered = energyAvailable; //the energy I am able to get from the raster
	    int newLevelOfEnergy=myEnergy+energyGathered*simpleAgent.getGainFromFood(); //my new level of energy

	    simpleAgent.setEnergy(newLevelOfEnergy);  //update agent

	    world->setValue(_rasterId, agent.getPosition(), energyAvailable - energyGathered  ); //update raster 
	}
}

std::string EatAction::describe() const
{
	return "Eat action";
}

} // namespace Examples


