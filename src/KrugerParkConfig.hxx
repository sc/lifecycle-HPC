
#ifndef __KrugerParkConfig_hxx__
#define __KrugerParkConfig_hxx__

#include <Config.hxx>

namespace Examples
{

class KrugerParkConfig : public Engine::Config
{	
	int _lionNumber;
	int _impalaNumber;

	int _lionReproduce;
	int _impalaReproduce;

	int _impalaSdSpeed ;
	int _impalaMeanSpeed ;
        
        int _impalaSdLifeExpectancy ;
        int _impalaMeanLifeExpectancy ;

        int _impalaGainFromFood ;

	int _lionSdSpeed ;
	int _lionMeanSpeed ;

        int _lionSdLifeExpectancy ;
        int _lionMeanLifeExpectancy ;

        int _lionGainFromFood ;

        int _grassGrowthRate ;
public:
	KrugerParkConfig( const std::string & xmlFile );
	virtual ~KrugerParkConfig();

	void loadParams();

	friend class KrugerPark;
};

} // namespace Examples

#endif // __KrugerParkConfig_hxx__

